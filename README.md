# Vuepress-documents-parser-cli

This script parses markdown files, from an Vuepress project, into a json file in  `./.vuepress/public/document.json` as needed by the 
[vuepress-pugin-offlinesearch](https://www.npmjs.com/package/@datev/vuepress-plugin-offlinesearch) to build its search index.
## Usage

* Add the plugin as dependency:

``` sh
npm i @datev/vuepress-documents-parser-cli
```

After that you just type this into the console.

``` sh
npm run generate-documents
```

The following json structure will be build:

```json
[
  {
    "pageTitle": "exampleTitle",
    "sectionHeader": "ExampleHeader",
    "sectionSlug": "/exampleSite.html#exampleSection",
    "sectionContent": "This is example content"
  }
]
```

## Contributing 

Contributions are welcome !

 
If you want to contribute or have a question please just open a new issue.




