#! /usr/bin/env node
const recursive = require("recursive-readdir");
const fs = require('fs');
const path = require('path');
const slugify = require('@vuepress/shared-utils/lib/slugify');

recursive("./", ["!*.md", 'node_modules/**']).then(
    function (files) {
        let results = [];
        console.info('Found ' + files.length + ' files. Beginning to parse...');
        for (let file of files) {
            console.info('Parsing file: ' + file);
            results = results.concat(parseFile(file));
        }
        console.info('Writing output file...');
        fs.writeFileSync(
            './.vuepress/public/documents.json',
            JSON.stringify(results, null, 2) //pretty-prints the JSON string
        );
        console.info("Done.")
    },
    function (error) {
        console.error(error)
    }
);

function parseFile(file) {
    let documentAsString = fs.readFileSync(file, "utf8");
    let pageTitle = extractPageTitle(documentAsString);

    let documentAsArray = documentAsString.split('\n#'); //TODO, check for beginning of line with following whitespace
    let result = [];

    for (let currentSection of documentAsArray) {
        if (currentSection !== '') {
            currentSection.trim();

            let sectionHeader = extractSectionHeader(currentSection);
            let sectionContent = extractSectionContent(currentSection);
            let sectionSlug = extractSectionSlug(file, sectionHeader);

            let sectionObject = {
                pageTitle: pageTitle,
                sectionHeader: sectionHeader,
                sectionSlug: sectionSlug,
                sectionContent: sectionContent
            };
            result.push(sectionObject);
        }
    }
    return result;
}

function extractPageTitle (documentAsString) {
    return removeLeadingHashtags(documentAsString.substring(0, documentAsString.indexOf('\n')));
}

function extractSectionHeader(section){
    let sectionHeader = section.substring(0, section.indexOf('\n')).trim();
    return removeLeadingHashtags(sectionHeader);
}

function extractSectionContent(section) {
    return section.substring(section.indexOf('\n'), section.length - 1).trim();
}

function extractSectionSlug(filePath, sectionHeader) {
    let sectionSlug = '/' + filePath.substring(0, filePath.length - 3); // drop .md extension;
    if (path.basename(sectionSlug) === 'README') {
        sectionSlug = sectionSlug.replace('README', 'index') //replace README with index or links will break
    }
    let anchor = '#'+ slugify(sectionHeader);
    if (anchor !== '#') {
        sectionSlug = sectionSlug + anchor
    }
    return sectionSlug;
}

function removeLeadingHashtags (inputString) {
    while (inputString.charAt(0) === '#') {
        inputString = inputString.substring(1);
    }
    return inputString.trim()
}
